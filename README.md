A logger node for ROS-lunar. Instead of writing the logs to files, the built-in /rousout is used. 
Its offers way more performance and can be easily used with other nodes.
The logger takes 1-3 parameters on following order:

Option:
	determines type of the logged message:
	W: Warning
	D: Debug
	F; Fatal
	E: Error,
	I: Info (default)

Message:
	the message to be logged, its being converted to string but should be a string to start with for speed issues and compatibility

Once:
	True: Messages with same content are only logged once
	False: Log all the things, all the time

Code:
	log_it(option='i', message, once=False)

Ros-Topic:
	"logs"

the basic log ('rostopic echo logs') can be modified ('rostopic echo logs') to view logged messages only.

The configurationfile for the logger can be found @ '$ROS_ROOT/../../etc/ros/python_logging.conf'
The logfile itself can be found with 'roslaunch-logs'
