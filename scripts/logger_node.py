#!/usr/bin/env python
import rospy
from std_msgs.msg import String

# Loggingtabelle von Ros

#            Debug   Info    WArn    Error   Fatal
# tdout                X
# stderr                       X       X       X
# logfile     X        X       X       X       X
# /rosout     o        X       X       X       X


class LoggerNode(object):
    def __init__(self):
        self.Subscriber = rospy.Subscriber('logs', String, self.callback)

    def callback(self, data):
        #rospy.loginfo(rospy.get_caller_id() + ' ' + data.data)
        self.log_it('i',    # Modi: Info, Debug, Warning, Fatal, Error 
                data.data,  # daten aus dem Callback
                True)       # Wenn gesetzt wird nur 1mal geloggt
    def start(self):        # start spinning
        rospy.spin()

    def log_it(self, option='i', message, once=False):
        if (once==False):
            if (option=='w'):
                self.log_warning(message)
            elif (option=='d'):
                self.log_debug(message)
            elif (option=='f'):
                self.log_fatal(message)
            elif (option=='e'):
                self.log_error(message)
            else:
                self.log_info(message)
        else:
            self.log_once(option, message)


    def log_info(self, message):
        rospy.loginfo(message)
        
    def log_debug(self, message):
        rospy.logdebug(message)

    def log_warning(self, message):
        rospy.logwarn(message)

    def log_info(self, message):
        rospy.loginfo(message)

    def log_error(self, message):
        rospy.logerr(message)

    def log_fatal(self, message):
        rospy.logfatal(message)

    def log_once(self, option, message):
        if (option=='w'):
            rospy.logwarn_once(message)
        elif (option=='d'):
            rospy.logdebug_once(message)
        elif (option=='f'):
            rospy.logfatal_once(message)
        elif (option=='e'):
            rospy.logerr_once(message)
        else :
            rospy.loginfo_once(message)

if __name__ == '__main__':
    rospy.init_node("LoggerNode", anonymous=False)
    node = LoggerNode()
    node.start()
