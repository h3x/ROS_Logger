#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def timestamp(seconds=False):
    if (seconds==False):
        now = rospy.get_rostime()
        return ("time: %i" % now.nsecs)
    else:
        seconds = rospy.get_time()
        return ("time: %.2f" % seconds)
        
def callback(data):
    rospy.loginfo(rospy.get_caller_id() + '%s @ %s',data.data, timestamp(True))
    fd.write("a")

def listener():
    global fd
    filename = "jetson.txt"
    fd = open('jetson.txt', "w")
    rospy.init_node('logger', anonymous=False)
    rospy.Subscriber('logs', String, callback)
    # spin() simply keeps python from exiting until this node is stopped
    try:
        rospy.spin()
    finally:
        fd.close()
if __name__ == '__main__':
    listener()
